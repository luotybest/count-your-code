# One script to count your total lines of your project
**[中文](https://gitee.com/luotybest/count-your-code/blob/master/README.zh.md)**
## first of all
I code this demo on my windows system,if yours is linux or Mac, you need to put the executable file count to your path:
ls -n <your-project-name>/node_modules/.bin/count /usr/bin/   (/usr/local/bin)

**note**: when you are link the program count to your path, the path of `count` must be a absolute path

## usage
```bash
npm install count-your-code -g

count -t <type_list...> -d <directory_list...> -n [true/false]
    # -t: tell the program what type of file you want to count
    # -d: tell the program which file or directory you want to count
    # -n: tell the program if you want the directory `node_modules` to be included
```