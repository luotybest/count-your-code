# 一个计算代码行数的JavaScript小应用

## 首先
本在是在windows系统中进行的开发，如果你是Linux或者Mac系统，下载后可能不能立即使用，需要将`node_modules`下的.bin文件夹中的`count` 添加到你的环境变量中：
ln -s <your-project-path>/node_modules/.bin/count /usr/bin/


**注意**: 链接 count 到环境变量中时需要链接count的绝对路径

## 使用方法

```bash
npm install count-your-code -g

count -t 你想计算的文件类型（如js、vue，默认js） -d 你要计算的目录（默认 . ，即当前目录） -n 是否排除`node_modules`(默认值为true，可选参数为true/false)
```